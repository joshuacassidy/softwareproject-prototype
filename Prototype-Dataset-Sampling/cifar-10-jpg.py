import os, shutil
from keras.datasets import cifar10
import numpy as np
from PIL import Image, ImageOps

output_dir = "cifar10_sample"
try:
    shutil.rmtree(output_dir)
except:
    pass

def save_image(filename, data_array):
    im = Image.fromarray(data_array.astype('uint8'))
    ImageOps.invert(im).save(filename)

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

if not os.path.exists(output_dir):
    os.mkdir(output_dir)

cifar10_label_names = {
    0: "airplane", 
    1: "automobile",
    2: "bird",
    3: "cat",
    4: "deer",
    5: "dog",
    6: "frog",
    7: "horse",
    8: "ship",
    9: "truck",
}
dir_types = ['train', 'test', 'validation']

for dir_type in dir_types:
    folder_name = os.path.join(output_dir, dir_type)
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)

def is_sample_label(label):
    return label == 0 or label == 1 or label == 9

for dir_type in dir_types:
    for i in range(len(x_train)):
        label = y_train[i][0]
        if is_sample_label(label):
            label_name = cifar10_label_names[label]
            folder_name = os.path.join(output_dir, dir_type, label_name)
            if not os.path.exists(folder_name):
                os.mkdir(folder_name)

def populate_directory(start_range, end_range, data, output_dir, data_type, cifar10_label_names, y_train, limit):
    count = 0
    for i in range(start_range, end_range):
        if count >= limit:
            break
        label = y_train[i][0]
        if is_sample_label(label):
            count += 1
            label_name = cifar10_label_names[y_train[i][0]]
            filename = "{i}.jpg".format(i=i)
            file_url = os.path.join(output_dir, data_type, label_name, filename)
            save_image(file_url, data[i])

populate_directory(6000, len(x_train), x_train, output_dir, 'train', cifar10_label_names, y_train, 1000)
populate_directory(0, 6000, x_train, output_dir, 'validation', cifar10_label_names, y_train, 100)
populate_directory(0, len(x_test), x_test, output_dir, 'test', cifar10_label_names, y_test, 100)
