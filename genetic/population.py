import random
from . import genes
from .member import Member

class Population:

    def __init__(self, size, generate_new_members, base_dir):
        self.size = size
        self.members = []
        for _ in range(size):
            if generate_new_members == True:
                new_member = Member(new_member=True, base_dir=base_dir)
                self.members.append(new_member)
            else:
                new_member = Member(new_member=False, base_dir=base_dir)
                self.members.append(new_member)

    def get_fittest_member(self):
        fittest = self.members[0]
        for i in self.members:
            if i.fitness > fittest.fitness:
                fittest = i
        return fittest

    def __str__(self):
        pop_str = 'Population(members='
        for mem in self.members:
            pop_str += mem
        pop_str += ')'
        return pop_str

