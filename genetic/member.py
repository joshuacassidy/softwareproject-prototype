import random, math
from . import genes
import datetime
import os, shutil
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import keras
from keras.preprocessing.image import ImageDataGenerator
from .utils import net_generator



class Member:

    def __init__(self, new_member, base_dir):
        self.layers = []
        self.nodes = []
        self.fitness = 0
        self.prob = 0
        self.base_dir = base_dir
        if new_member == True:
            # print('generating new member')
            self.generate()
        else:
            pass
            # print('breeding member')

    def generate(self):
        for _ in range(genes.LAYERS):
            self.layers.append(random.randint(0, 1))
        is_null = True
        for layer in self.layers:
            if layer == 1:
                is_null = False
                break

        if is_null:
            self.layers[len(self.layers) - 1] = 1

        layers_byte_string = '0b' + (''.join(str(i) for i in self.layers))
        layers_value = int(layers_byte_string, 2)

        for i in range(layers_value):
            temp_nodes = []
            for _ in range(genes.NODES):
                temp_nodes.append(random.randint(0, 1))
            is_null = True
            for node in temp_nodes:
                if node == 1:
                    is_null = False
                    break
            if is_null:
                temp_nodes[len(temp_nodes) - 1] = 1
            self.nodes.append(temp_nodes)
        self.nodes = net_generator.validate_nodes(layers=self.layers, nodes=self.nodes)
        self.evaluation()

    def evaluation(self):
        from . import net_template
        layers = ""
        layers_template = "model.add(Conv2D({nodes}, (3, 3), activation='relu'))"

        layers_byte_string = '0b' + (''.join(str(i) for i in self.layers))
        layers_value = int(layers_byte_string, 2)
        # print("stack trace nodes:", self.nodes)
        # print("stack trace layers:", self.layers)
        for i in range(0, layers_value):
            if i != 0:
                layers += "    "
            nodes_steps_byte_string = '0b' + (''.join(str(j) for j in self.nodes[i]))
            nodes_value = int(nodes_steps_byte_string, 2)
            layers += layers_template.format(nodes=nodes_value) + "\n"
        
        start = datetime.datetime.now()

        train_dir = os.path.join(self.base_dir, 'train')

        validation_dir = os.path.join(self.base_dir, 'validation')

        test_dir = os.path.join(self.base_dir, 'test')

        network_code = net_template.code.format(layers=layers)

        execute_net_code = """create_neural_net(
            base_dir="{base_dir}", 
            train_dir="{train_dir}", 
            validation_dir="{validation_dir}", 
            test_dir="{test_dir}"
        )""".format(
            base_dir=self.base_dir, 
            train_dir=train_dir, 
            validation_dir=validation_dir,
            test_dir=test_dir
        )
        context = {}
        exec(network_code + "\ntest_loss, test_acc = " + execute_net_code, context)
        end = datetime.datetime.now()

        self.loss = context['test_loss']
        self.acc = context['test_acc']
        self.compile_time = end - start
        self.fitness = math.exp(math.exp(math.exp(self.acc - self.loss)))
        # print('layers:', self.layers)
        # print('nodes:', self.nodes)
        # print('loss:', self.loss)
        # print('acc:', self.acc)

        return self.fitness
    
    def __str__(self):
        return """Member(
                    layers={layers} 
                    nodes={nodes}, 
                    fitness={fitness}, 
                    loss={loss}, 
                    acc={acc},
                    compile_time={compile_time}
                )""".format(
                    layers=str(self.layers),
                    nodes=str(self.nodes),
                    fitness=str(self.fitness),
                    loss=str(self.loss),
                    acc=str(self.acc),
                    compile_time=str(self.compile_time)
                )


