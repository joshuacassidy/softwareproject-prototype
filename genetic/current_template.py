
import datetime
from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import os
import keras
import os, shutil
from keras.preprocessing.image import ImageDataGenerator 
def create_neural_net(train_datagen, validation_datagen, base_dir, train_dir, validation_dir, test_dir):
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                    activation='relu',
                    input_shape=(28, 28, 3)))
    model.add(Conv2D(148, (3, 3), activation='relu'))

    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(10, activation='softmax'))

    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy']
    )
    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(28, 28),
        batch_size=50,
        class_mode='categorical'
    )

    validation_generator = train_datagen.flow_from_directory(
        validation_dir,
        target_size=(28, 28),
        batch_size=50,
        class_mode='categorical'
    )

    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_directory(
        test_dir,
        target_size=(28, 28),
        batch_size=10,
        class_mode='categorical'
    )

    history = model.fit_generator(
        train_generator, 
        steps_per_epoch=10, 
        epochs=1, 
        validation_data=validation_generator,
        validation_steps=5,
        workers=7,
        max_queue_size=20,
    )

    test_loss, test_acc = model.evaluate_generator(
        test_generator, 
        steps=20,
        workers=5
    )
    
    return (test_loss, test_acc)

    model.save('mnist.h5')
