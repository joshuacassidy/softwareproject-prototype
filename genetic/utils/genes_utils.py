def genes_to_int(genes):
    genes_string = '0b' + (''.join(str(i) for i in genes))
    genes_int = int(genes_string, 2)
    return genes_int
