import random
from .. import genes as genes

def validate_layers(layers):
    is_null = True
    for layer in layers:
        if layer == 1:
            is_null = False
            break

    if is_null:
        layers[len(layers) - 1] = 1
    return layers

def validate_nodes(layers, nodes):
    mutated_layers = validate_layers(layers)

    layers_byte_string = '0b' + (''.join(str(i) for i in mutated_layers))
    layers_value = int(layers_byte_string, 2)

    for node in nodes:
        for i in node:
            is_null = True
            if i == 1:
                is_null = False
                break
        if is_null:
            node[len(node) - 1] = 1

    return nodes

