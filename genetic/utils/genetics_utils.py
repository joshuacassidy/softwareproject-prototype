from .. import genes as genes
import random

def generate_child_layers(child, parent1, parent2):
    for i in range(genes.LAYERS):
        if random.uniform(0, 1) < genes.CROSSOVER_RATE:
            child.layers.append(parent1.layers[i])
        else:
            child.layers.append(parent2.layers[i])

def generate_child_nodes(child, parent1, parent2, layers_amount):
    for layer in range(layers_amount):
        parent1_nodes = parent1.nodes[layer % len(parent1.nodes)]
        parent2_nodes = parent2.nodes[layer % len(parent2.nodes)]
        temp_nodes = []
        for i in range(genes.NODES):
            if random.uniform(0, 1) < genes.CROSSOVER_RATE:
                temp_nodes.append(parent1_nodes[i])
            else:
                temp_nodes.append(parent2_nodes[i])
        child.nodes.append(temp_nodes)
