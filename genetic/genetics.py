import random
from . import genes
from .population import Population
from .member import Member
from .utils import net_generator, genetics_utils, genes_utils

class Genetics:

    def __init__(self, base_dir=None):
        self.base_dir = base_dir

    def evolve_population(self, population, base_dir):
        self.base_dir = base_dir
        next_generation = Population(population.size, generate_new_members=False, base_dir=self.base_dir)
        for i in range(population.size):
            parent1 = self.get_fittest(population)
            parent2 = self.get_fittest(population)
            child = self.crossover(parent1, parent2)
            next_generation.members[i] = child
        for i in range(population.size):
            self.mutate(next_generation.members[i])
        # print('successfully evolved')

        # for i in population.members:
        #     print('nodes: ', i.nodes)
        #     print('layers: ', i.layers)

        return next_generation

    def crossover(self, parent1, parent2):
        child = Member(new_member=False, base_dir=self.base_dir)
        genetics_utils.generate_child_layers(child=child, parent1=parent1, parent2=parent2)
        child.layers = net_generator.validate_layers(layers=child.layers)
        layers_amount = genes_utils.genes_to_int(child.layers)

        for layer in range(layers_amount):
            temp_nodes = []
            parent1_nodes = parent1.nodes[layer % len(parent1.nodes)]
            parent2_nodes = parent2.nodes[layer % len(parent2.nodes)]
            for i in range(genes.NODES):
                if random.uniform(0, 1) < genes.CROSSOVER_RATE:
                    temp_nodes.append(parent1_nodes[i])
                else:
                    temp_nodes.append(parent2_nodes[i])
            child.nodes.append(temp_nodes)
        child.nodes = net_generator.validate_nodes(layers=child.layers, nodes=child.nodes)
        # print("new child")
        # print("new child layers", child.layers)
        # print("new child nodes", child.nodes)
        return child


    def mutate(self, member):
        for nodes in member.nodes:
            for i in range(len(nodes)):
                if random.uniform(0, 1) < genes.MUTATION_RATE:
                    if nodes[i] == 1:
                        nodes[i] = 0
                    else:
                        nodes[i] = 1
        member.nodes = net_generator.validate_nodes(layers=member.layers, nodes=member.nodes)
        
    def get_fittest(self, population):
        
        tournament_size = population.size
        fittest = random.choice(population.members)

        for _ in range(tournament_size):
            current = random.choice(population.members)
            if current.fitness > fittest.fitness:
                fittest = current
        return fittest

    def __str__(self):
        return 'Population()'

