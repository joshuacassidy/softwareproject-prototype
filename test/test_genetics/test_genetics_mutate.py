import unittest
from genetic.genetics import Genetics
import unittest.mock as mock
import random
from genetic.member import Member

class TestGenetics(unittest.TestCase):

    def test_mutate_seeded(self):
        random.seed(15378586)
        child = Member(new_member=False, base_dir=None)
        child.layers = [1, 1, 0]
        child.nodes = [[1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 0]]

        genetics = Genetics()
        genetics.mutate(child)
        self.assertEqual([[1, 0, 1, 1, 0, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 0, 0]], child.nodes)

    
    


if __name__ == '__main__':
    unittest.main()