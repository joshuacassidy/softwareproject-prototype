import unittest
from genetic.genetics import Genetics
import unittest.mock as mock
from random import choice
import random
from genetic.member import Member

class TestGenetics(unittest.TestCase):

    @mock.patch('random.uniform', return_value=0.9)
    def test_crossover_parent_1(self, uniform_mock):
        parent1 = Member(new_member=False, base_dir=None)
        parent1.layers = [0, 0, 1]
        parent1.nodes = [
            [ 1, 1, 1, 1, 0, 1, 1,0 ],
        ]
        parent2 = Member(new_member=False, base_dir=None)
        parent2.layers = [0, 1, 1]
        parent2.nodes = [
            [ 0, 1, 1, 1, 1, 1, 1,0 ],
            [ 1, 0, 1, 1, 1, 1, 1,0 ],
            [ 1, 1, 0, 1, 1, 1, 1,0 ],
        ]
        genetics = Genetics()
        child = genetics.crossover(parent1, parent2)
        self.assertEqual(child.layers, [0, 1, 1])
        self.assertEqual(child.nodes, parent2.nodes)


    @mock.patch('random.uniform', return_value=0.0)
    def test_crossover_parent_2(self, uniform_mock):
        parent1 = Member(new_member=False, base_dir=None)
        parent1.layers = [0, 1, 0]
        parent1.nodes = [
            [ 1, 1, 1, 1, 0, 1, 1,0 ],
        ]
        parent2 = Member(new_member=False, base_dir=None)
        parent2.layers = [1, 1, 0]
        parent2.nodes = [
            [ 0, 1, 1, 1, 1, 1, 1,0 ],
            [ 1, 0, 1, 1, 1, 1, 1,0 ],
            [ 1, 1, 0, 1, 1, 1, 1,0 ],
        ]
        genetics = Genetics()
        child = genetics.crossover(parent1, parent2)
        self.assertEqual(child.layers, parent1.layers)
        self.assertEqual(child.nodes, [
            [ 1, 1, 1, 1, 0, 1, 1,0 ],
            [ 1, 1, 1, 1, 0, 1, 1,0 ],
        ])


    def test_crossover_seeded(self):
        random.seed(15378586)
        parent1 = Member(new_member=False, base_dir=None)
        parent1.layers = [0, 1, 0]
        parent1.nodes = [
            [ 1, 1, 1, 1, 0, 1, 1,0 ],
        ]
        parent2 = Member(new_member=False, base_dir=None)
        parent2.layers = [0, 1, 1]
        parent2.nodes = [
            [ 0, 1, 1, 1, 1, 1, 1,0 ],
            [ 1, 0, 1, 1, 1, 1, 1,0 ],
            [ 1, 1, 0, 1, 1, 1, 1,0 ],
        ]
        genetics = Genetics()
        child = genetics.crossover(parent1, parent2)
        self.assertEqual(child.layers, [0, 1, 0])
        self.assertEqual(child.nodes, [[0, 1, 1, 1, 1, 1, 1, 0], [1, 0, 1, 1, 1, 1, 1, 0]])
        
    

if __name__ == '__main__':
    unittest.main()