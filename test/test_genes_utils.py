# python3 -m unittest discover tests
import unittest, random
import genetic.utils.genes_utils as genes_utils


class TestGenetics(unittest.TestCase):

    def test_genes_to_int(self):
        input_genes = [
            [0, 1, 1, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1], [0, 1, 1, 1, 0, 1, 0], 
            [0, 0, 0, 0, 1, 0, 1], [1, 0, 0, 1, 1, 1, 1], [0, 1, 0, 0, 0, 0, 0], 
            [0, 1, 1, 1, 1, 1, 0], [0, 0, 1, 1, 0, 1, 1], [0, 0, 0, 0, 0, 0, 0], 
            [0, 0, 0, 0, 0, 1, 1], [1, 1, 1, 1, 1, 1, 1], [0, 0, 0, 1, 1, 1, 1],
            [1, 1],                [1, 0],                [0, 1],
        ]

        expected_outputs = [
            56, 1, 58,
            5, 79, 32,
            62, 27, 0,
            3, 127, 15,
            3, 2, 1,
        ]

        for i in range(len(input_genes)):
            actual_output = genes_utils.genes_to_int(input_genes[i])
            self.assertEqual(actual_output, expected_outputs[i])


if __name__ == '__main__':
    unittest.main()
