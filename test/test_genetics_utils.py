import unittest, random
import genetic.utils.genetics_utils as genetics_utils
from genetic.member import Member


class TestGenetics(unittest.TestCase):

    def test_generate_child_layers(self):
        child = Member(new_member=False, base_dir=None)
        parent1 = Member(new_member=False, base_dir=None)
        parent2 = Member(new_member=False, base_dir=None)
        random.seed(15378586)
        parent1.layers = [1, 0, 0]
        parent2.layers = [1, 1, 0]

        genetics_utils.generate_child_layers(child=child, parent1=parent1, parent2=parent2)
        self.assertEqual(child.layers, [1,0, 0])
        


if __name__ == '__main__':
    unittest.main()