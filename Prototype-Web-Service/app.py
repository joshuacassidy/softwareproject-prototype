from flask import Flask, Response, request
import json
import pymongo,math
from bson.json_util import dumps

app = Flask(__name__)


client = pymongo.MongoClient("mongodb+srv://josh:josh@cluster0-eznmu.mongodb.net/prototype?retryWrites=true&w=majority")
db = client.prototype

def genes_to_int(genes):
    genes_string = '0b' + (''.join(str(i) for i in genes))
    genes_int = int(genes_string, 2)
    return genes_int


@app.route('/networks', methods=['GET'])
def get_networks():
    records = []

    

    for record in db.all_generated_cnns.find({}):
        for i in range(len(record['nodes'])):
            record['nodes'][i] = genes_to_int(record['nodes'][i])
            
        record['layers'] = genes_to_int(record['layers'])  
        records.append(record)
    response = dumps(records)
    return Response(response, mimetype='application/json', status='200')

@app.route('/add_best_network', methods=['POST'])
def add_best_network():

    generation = None
    nodes = None
    layers = None
    fitness = None
    acc = None
    loss = None
    compile_time = None
    try:
        generation = json.loads(request.data)["generation"]
        nodes = json.loads(request.data)["nodes"]
        layers = json.loads(request.data)["layers"]
        fitness = json.loads(request.data)["fitness"]
        acc = json.loads(request.data)["acc"]
        loss = json.loads(request.data)["loss"]
        compile_time = json.loads(request.data)["compile_time"]
    except:
        response = "Bad Input generation={generation}, nodes={nodes}, layers={layers}, fitness={fitness}, acc={acc}, loss={loss}, compile_time={compile_time}".format(
            generation = generation, 
            nodes = nodes, 
            layers = layers, 
            fitness = fitness, 
            acc = acc, 
            loss = loss, 
            compile_time = compile_time
        )
        return Response(json.dumps({'message': response}), mimetype='application/json', status='400')
    record = {
        "generation": generation, 
        "nodes": nodes, 
        "layers": layers, 
        "fitness": fitness, 
        "acc": acc, 
        "loss": loss, 
        "compile_time": compile_time
    }
    db.best_generated_cnns.insert_one(record)

    return Response(json.dumps({'message': "Success added new record!"}), mimetype='application/json', status='201')

@app.route('/add_network', methods=['POST'])
def add_network():

    generation = None
    nodes = None
    layers = None
    fitness = None
    acc = None
    loss = None
    compile_time = None
    try:
        generation = json.loads(request.data)["generation"]
        nodes = json.loads(request.data)["nodes"]
        layers = json.loads(request.data)["layers"]
        fitness = json.loads(request.data)["fitness"]
        acc = json.loads(request.data)["acc"]
        loss = json.loads(request.data)["loss"]
        compile_time = json.loads(request.data)["compile_time"]
    except:
        response = "Bad Input generation={generation}, nodes={nodes}, layers={layers}, fitness={fitness}, acc={acc}, loss={loss}, compile_time={compile_time}".format(
            generation = generation, 
            nodes = nodes, 
            layers = layers, 
            fitness = fitness, 
            acc = acc, 
            loss = loss, 
            compile_time = compile_time
        )
        return Response(json.dumps({'message': response}), mimetype='application/json', status='400')
    record = {
        "generation": generation, 
        "nodes": nodes, 
        "layers": layers, 
        "fitness": fitness, 
        "acc": acc, 
        "loss": loss, 
        "compile_time": compile_time
    }
    db.all_generated_cnns.insert_one(record)

    return Response(json.dumps({'message': "Success added new record!"}), mimetype='application/json', status='201')


if __name__ == '__main__':
    app.run(debug=True)