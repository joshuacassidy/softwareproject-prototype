from .genetic.genetics import Genetics
from .genetic.population import Population
from .genetic.member import Member
import random
from numpy.random import seed
import json
import requests

def generate_networks(base_dir):
    seed(15378586)
    random.seed(15378586)
    generation = 0


    __genetics = Genetics()
    __population = Population(3, generate_new_members=True, base_dir=base_dir)

    print("Population: ")
    for mem in __population.members:
        # print(mem)
        record = { 
            "generation": generation,
            "nodes": mem.nodes,
            "layers": mem.layers,
            "fitness": mem.fitness,
            "acc": mem.acc,
            "loss": mem.loss,
            "compile_time": int(mem.compile_time.seconds)
            }

        payload = json.dumps(record)
        # print(payload)
        response = requests.post(url="https://ncirl-final-year-project.herokuapp.com/add_network", data=payload)
        # print(response)
        # print(response.json())


    for i in range(0, 3):
        # print("Population: ")
        print("Generation:" + str(i))
        print("The fittest member is: " + str(__population.get_fittest_member()) + "\n")

        __population = __genetics.evolve_population(__population, base_dir=base_dir)
        for member in __population.members:
            
            member.evaluation()
            record = { 
            "generation": generation,
            "nodes": member.nodes,
            "layers": member.layers,
            "fitness": member.fitness,
            "acc": member.acc,
            "loss": member.loss,
            "compile_time": int(mem.compile_time.seconds)
            }

            payload = json.dumps(record)

            response = requests.post(url="https://ncirl-final-year-project.herokuapp.com/add_network", data=payload)
            # print(response)
            # print(payload)
            # print(response.json())

        generation = i + 1
        
    for mem in __population.members:
        

        # print(mem)
        record = { 
            "generation": generation,
            "nodes": mem.nodes,
            "layers": mem.layers,
            "fitness": mem.fitness,
            "acc": mem.acc,
            "loss": mem.loss,
            "compile_time": int(mem.compile_time.seconds)
            }

        payload = json.dumps(record)

        response = requests.post(url="https://ncirl-final-year-project.herokuapp.com/add_network", data=payload)
        # print(response)
        # print(response.json())


    best = __population.get_fittest_member()
    record = { 
        "generation": generation,
        "nodes": best.nodes,
        "layers": best.layers,
        "fitness": best.fitness,
        "acc": best.acc,
        "loss": best.loss,
        "compile_time": int(best.compile_time.seconds)
    }

    payload = json.dumps(record)

    response = requests.post(url="https://ncirl-final-year-project.herokuapp.com/add_best_network", data=payload)
    # print(response)
    # print(response.json())

    print("The fittest member is: " + str(__population.get_fittest_member()) + "\n")
        
    # print("\n\nThe final generation is " + str(generation) + ". The fittest member is: " + str(__population.get_fittest_member()) + " and has a fitness of " + str(__population.get_fittest_member().fitness) + "\n")